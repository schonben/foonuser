<?php

class mailer {
    protected $from = "list@uplink.fi";
    protected $to = "";
    protected $subject = "";

    public function __construct($to="") {
        if (static::isEmail($to)) {
            $this->to = $to;
        }
    }

    // Getters


    // Setters
    public function setFrom($email) {
        if (static::isEmail($email)) {
            $this->from = $email;
        }
    }
    public function setTo($email) {
        if (static::isEmail($email)) {
            $this->to = $email;
        }
    }
    public function setSubject($subject) {
        $this->subject = $subject;
    }

    public function send ($message,$subject="",$to="",$from="") {
        // Fill the variables.
        if (empty($subject)) $subject = $this->subject;
        if (empty($to)) $to = $this->to;
        if (empty($from)) $from = $this->from;

        // Set headers
        $headers =
            "From: {$from}\r\n" .
            "Reply-To: {$from}\r\n" .
            "X-Mailer: Foon PHP mailer - PHP/" . phpversion();

        // Send mail
        mail($to, $subject, $message, $headers);
    }

	static function isEmail($email) {
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}


}

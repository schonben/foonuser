<?php

require "mailer.class.php";

class user {
    protected $id = null;
    protected $fname = null;
    protected $lname = null;
    protected $refresh;
    protected $query;
    protected $timeout;
    protected $schema;
    const PWSTR = 10;

    public function __construct($token="") {
        // Set default timeout
        $this->timeout = 3600;

        $this->schema = static::getSchema();

        // Prepare a query that might be used a lot.
        $sql = "
          SELECT
            *
          FROM
            {$this->schema}.users
          WHERE
            id = :uuid
            ";
        $this->query = $GLOBALS['db']->prepare($sql);

        // Attempt a token login if defined
        if (!empty($token)) {
            $this->tokenLogin($token);
        }
    }

    // Getters
    public function getId() {
        if (empty($this->id)) {
            return false;
        } else {
            return $this->id;
        }
    }
    public function getRefresh($milli=false) {
        if (empty($this->id)) {
            return false;
        } else {
            return ($milli)?$this->refresh*1000:$this->refresh;
        }
    }
    public function getName() {
        return $this->fname." ".$this->lname;
    }
    public function getFirstName() {
        return $this->fname;
    }
    public function getLastName() {
        return $this->lname;
    }

    // Setters
    public function setFirstName($name) {
        $this->fname = $name;
    }
    public function setLastName($name) {
        $this->lname = $name;
    }
    public function setRefresh($time) {
        $this->refresh = $time;
    }

    public function save() {
        $sql = "
            UPDATE
              {$this->schema}.users
            SET
              fname = :fname,
              lname = :lname,
              refresh = :refresh
            WHERE
              id = :uuid
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(
            ":fname" => $this->fname,
            ":lname" => $this->lname,
            ":refresh" => $this->refresh,
            ":uuid" => $this->id,
        ));
    }

    public function changePassword($old,$new) {
        // Fetch user information based on id
        $sql = "
          SELECT
            *
          FROM
            {$this->schema}.users
          WHERE
            id = :uuid
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":uuid" => $this->id));
        $row = $query->fetch();

        if (crypt($old, $row['passwd']) == $row['passwd']) {
            // Old password matches
            return $this->updatePassword($new);
        }
        return false;
    }

    public function updatePassword($password,$rounds=5000) {
        // Check password strength
        if (static::passwordEntropy($password) >= self::PWSTR) {
            $salt = uniqid(mt_rand(), true);
            $passwd = crypt($password,'$5$rounds='.$rounds.'$'.$salt.'$');
            $sql = "
                UPDATE
                  {$this->schema}.users
                SET
                  passwd = :passwd
                WHERE
                  id = :uuid
                ";
            $query = $GLOBALS['db']->prepare($sql);
            return $query->execute(array(":passwd" => $passwd,":uuid" => $this->id,)); // True or False
        }
        return false;
    }

    public function login($login,$password,$timeout=false) {
        $login = strtolower($login);
        // Fetch user information based on login
        $sql = "
          SELECT
            *
          FROM
            {$this->schema}.users
          WHERE
            email = :email
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":email" => $login));
        $row = $query->fetch();

        // Check if the password is correct.
        if (crypt($password, $row['passwd']) == $row['passwd']) {
            $this->id = $row['id'];
            $this->populate();

            if ($timeout !== false) {
                if ($timeout < 60) $timeout = 60;
                if ($timeout > 2592000) $timeout = 2592000;
                $this->timeout = $timeout;
            }

            return $this->getToken();
        }

        // Oh noes, the login has failed!
        return false;
    }

    public function tokenLogin($token) {
        if (count(explode(";",$token)) != 4) {
			return false;
		}
        list($uuid,$timestamp,$timeout,$hash) = explode(";",$token);
        // Check that it is a valid token format, and token is not expired
        if (empty($uuid) || empty($hash) || $timestamp+$timeout < time() || $timestamp > time() || $timeout > 2592000) {
            return false;
        }

        // Fetch the record from the database using the prepared query
        $this->query->execute(array(":uuid" => $uuid));
        $row = $this->query->fetch();

        // Check if token matches
        if (static::hmacToken($row['id'],$timestamp,$timeout,$row['passwd']) == $hash) {
            // Login successful
            $this->id = $row['id'];
            // Set timeout to the value that was used in the login token. New tokens will use this value.
            $this->populate();
            $this->timeout = $timeout;
            return true;
        }

        // If all else fails
        return false;
    }

    protected function populate () {
        if (empty($this->id)) {
            return false;
        }
        $this->query->execute(array(":uuid" => $this->id));
        $row = $this->query->fetch();

        $this->refresh = $row['refresh'];
        $this->fname = $row['fname'];
        $this->lname = $row['lname'];
    }

    public function getToken($timeout=0) {
        if (empty($this->id)) {
            return false;
        }
        $timestamp = time();

        // Set timeout if defined
        if (!empty($timeout)) {
            $this->timeout = $timeout;
        }

        // Limit timeout to 30 days.
        if ($this->timeout > 2592000) {
            $this->timeout = 2592000;
        }

        // Fetch the record from the database using the prepared query
        $this->query->execute(array(":uuid" => $this->id));
        $row = $this->query->fetch();
        // One more check to see that it's a real record
        if (empty($row['id'])) {
            return false;
        }

        // Return the token
        return static::hmacToken($row['id'],$timestamp,$this->timeout,$row['passwd'],true);
    }

    public function setCookie() {
        // Fetch the record from the database using the prepared query
        $this->query->execute(array(":uuid" => $this->id));
        $row = $this->query->fetch();

        if (!empty($row['id'])) {
            // Set the token as cookie
            setcookie("userToken",$this->getToken(),time()+7776000,"/");
            // Set login name as cookie
            setcookie("userLogin",$row['email'],time()+7776000,"/");
        }
    }

    public function newPasswd($password,$code) {
        if (!$this->tokenLogin($code)) {
            return "Invalid or expired link.";
        }
        $this->timeout = 3600;
        if (!$this->updatePassword($password)) {
            $this->id = null;
            return "Bad password.";
        }
    }

    public static function hmacToken ($id,$timestamp,$timeout,$key,$message=false) {
        $hash = hash_hmac("sha256",$id.$timestamp.$timeout,$key);
        return ($message)?$id.";".$timestamp.";".$timeout.";".$hash:$hash;
    }

    public static function createSecret ($login,$name) {
        // Create a temporary secret to be used to register, and send a mail.
        $login = strtolower($login);
        $schema = static::getSchema();

        if (!empty($name) && isEmail($login)) {
            $secret = uniqid(mt_rand(), true);

            // First check that login doesn't exist.
            if (static::loginExists($login)) {
                return "login exists";
            }

            // Delete possible old records
            $sql = "
                DELETE
                FROM
                  {$schema}.emailsecret
                WHERE
                  email = :email
                ";
            $query = $GLOBALS['db']->prepare($sql);
            $query->execute(array(
                ":email" => $login,
            ));

            // Mark the secret in the db
            $sql = "
            INSERT INTO {$schema}.emailsecret (
                email,
                name,
                secret
            ) VALUES (
                :email,
                :name,
                :secret
            )";
            $query = $GLOBALS['db']->prepare($sql);
            $query->execute(array(
                ":email" => $login,
                ":name" => $name,
                ":secret" => $secret,
            ));
            $code = crc32($secret.$login);

            $mailer = new mailer($login);
            $mailer->setSubject("List App Validation Code");
            $mailer->send("Enter this validation code: {$code}\n\nhttp://{$_SERVER['HTTP_HOST']}/validate?reg_login=".urlencode($login)."&reg_code=".urlencode($code));

            return "";
        }
        return "name or login empty.";
    }

    public static function register($login,$code,$password,$check) {
        // Register a new user with the provided login and password.
        $login = strtolower($login);
        $schema = static::getSchema();

        // First check that login doesn't exist.
        if (static::loginExists($login)) {
            return "login exists";
        }
        // Check that login and pass is good enough
        if (!isEmail($login)) {
            // Login invalid
            return "login invalid";
        }
        if ($password != $check) {
            return "passwords do not match";
        }
        if (static::passwordEntropy($password) < self::PWSTR) {
            // Password is too weak, stop sucking
            return "password too weak";
        }

        $sql = "
            SELECT
              *
            FROM
              {$schema}.emailsecret
            WHERE
              email = :email
              ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":email" => $login));
        $row = $query->fetch();

        if ($code != crc32($row['secret'].$login)) {
            return "invalid code";
        }
        list($fname,$lname) = explode(" ",$row['name'],2);

        // Ok, let's register
        $sql = "
            INSERT INTO {$schema}.users (
                email,
                fname,
                lname,
                passwd
            ) VALUES (
                :email,
                :fname,
                :lname,
                :passwd
            )";
        $query = $GLOBALS['db']->prepare($sql);
        $salt = uniqid(mt_rand(), true);
        $passwd = crypt($password,'$5$rounds=5000$'.$salt.'$');
        $query->execute(array(
            ":email" => $login,
            ":fname" => $fname,
            ":lname" => $lname,
            ":passwd" => $passwd,
            ));

        return "";
    }
    public static function recover ($email,$time=1800) {
        if (static::loginExists($email)) {
            $schema = static::getSchema();
            $sql = "
              SELECT
                *
              FROM
                {$schema}.users
              WHERE
                email = :email
                ";
            $query = $GLOBALS['db']->prepare($sql);
            $query->execute(array(":email" => $email));
            $record = $query->fetch();

            $recoveryHash = static::hmacToken($record['id'],time(),$time,$record['passwd'],true);
            list($uuid,$timestamp,$timeout,$hash) = explode(";",$recoveryHash);

            $mailer = new mailer($email);
            $mailer->setSubject("Password Recovery");
            $mailer->send("Password recovery link:\n\nhttp://{$_SERVER['HTTP_HOST']}/recover?code=".urlencode($recoveryHash)."\n\nLink is valid until ".date("H:i",$timestamp+$timeout));
            return "Link has been sent.";
        }
        return "User does not exist.";
    }
    public static function loginExists($login) {
        $login = strtolower($login);
        $schema = static::getSchema();
        $sql = "
          SELECT
            *
          FROM
            {$schema}.users
          WHERE
            email = :email
            ";
        $query = $GLOBALS['db']->prepare($sql);
        $query->execute(array(":email" => $login));
        if ($query->rowCount()) {
            // User exists
            return true;
        }
        return false;
    }

    public static function passwordEntropy($password,$raw=false) {
        $entropy = 0;
        if (preg_match("/[a-z]/",$password) == 1) {
            // Lower case 26
            $entropy += 26;
        }
        if (preg_match("/[A-Z]/",$password) == 1) {
            // Upper case 26
            $entropy += 26;
        }
        if (preg_match("/[0-9]/",$password) == 1) {
            // Digits 10
            $entropy += 10;
        }
        if (preg_match("/[^a-zA-Z0-9]/",$password) == 1) {
            // Others, we use 33 as a close enough value
            $entropy += 33;
        }
        $total = pow($entropy,mb_strlen($password));
        if ($raw)
            return $total;
        else
            return log10($total);
    }
    public static function users () {
        $schema = static::getSchema();
        $sql = "
            SELECT
              id,
              email,
              fname,
              lname
            FROM
              {$schema}.users
            ORDER BY
              fname,
              lname
            ";
        $query = $GLOBALS['db']->query($sql);
        $users = array();
        while ($row = $query->fetch()) {
            $users[$row['id']] = $row;
        }
        return $users;
    }
    public static function getSchema() {
        if (!empty($GLOBALS['setting']['userschema']) && ctype_alnum($GLOBALS['setting']['userschema'])) {
            return $GLOBALS['setting']['userschema'];
        }
        return "#schema#";
    }

}
